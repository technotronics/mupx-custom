#!/bin/bash

APPNAME=<%= appName %>
APP_PATH=/opt/$APPNAME
BUNDLE_PATH=$APP_PATH/current
UPLOAD_PATH=/opt/uploads/$APPNAME
ENV_FILE=$APP_PATH/config/env.list
PORT=<%= port %>

# Remove previous version of the app, if exists
set +e
docker rm -f $APPNAME

# We don't need to fail the deployment because of a docker hub downtime
set +e
docker pull humbertocruz/meteordgm:base

set -e
docker run \
  -d \
  --restart=always \
  --publish=$PORT:80 \
  --volume=$BUNDLE_PATH:/bundle \
  --volume=$UPLOAD_PATH:/upload \
  --env-file=$ENV_FILE \
  --hostname="$HOSTNAME-$APPNAME" \
  --name=$APPNAME \
  humbertocruz/meteordgm:base
